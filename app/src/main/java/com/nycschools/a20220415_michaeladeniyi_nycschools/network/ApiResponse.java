package com.nycschools.a20220415_michaeladeniyi_nycschools.network;

public class ApiResponse<T> {
    public T data;
    public Throwable error;
    public ApiResponse(T data){
        this.data = data;
        this.error = null;
    }
    public ApiResponse(Throwable error){
        this.error = error;
        this.data = null;
    }

    public T getData() {
        return data;
    }

    public void setData(T data) {
        this.data = data;
    }

    public Throwable getError() {
        return error;
    }

    public void setError(Throwable error) {
        this.error = error;
    }
}

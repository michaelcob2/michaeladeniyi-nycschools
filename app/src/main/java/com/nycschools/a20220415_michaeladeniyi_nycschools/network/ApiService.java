package com.nycschools.a20220415_michaeladeniyi_nycschools.network;

import com.nycschools.a20220415_michaeladeniyi_nycschools.network.model.SchoolModel;
import com.nycschools.a20220415_michaeladeniyi_nycschools.network.model.SchoolSatModel;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface ApiService {

    @GET("s3k6-pzi2.json")
    Call<List<SchoolModel>> getSchoolsInNyc();

    @GET("f9bf-2cp4.json")
    Call<List<SchoolSatModel>> getSchoolSatInNycByDBN(@Query("dbn") String dbn);

}

package com.nycschools.a20220415_michaeladeniyi_nycschools.network.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class SchoolModel {
    @Expose
    @SerializedName("dbn")
    private String dbn;

    @Expose
    @SerializedName("school_name")
    private String schoolName;

    @Expose
    @SerializedName("overview_paragraph")
    private String overviewParagraph;

    @Expose
    @SerializedName("website")
    private String websiteUrl;

    public String getDbn() {
        return dbn;
    }

    public void setDbn(String dbn) {
        this.dbn = dbn;
    }

    public String getSchoolName() {
        return schoolName;
    }

    public void setSchoolName(String schoolName) {
        this.schoolName = schoolName;
    }

    public String getOverviewParagraph() {
        return overviewParagraph;
    }

    public void setOverviewParagraph(String overviewParagraph) {
        this.overviewParagraph = overviewParagraph;
    }

    public String getWebsiteUrl() {
        return websiteUrl;
    }

    public void setWebsiteUrl(String websiteUrl) {
        this.websiteUrl = websiteUrl;
    }
}

package com.nycschools.a20220415_michaeladeniyi_nycschools.ui.schoolsInNyc;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MediatorLiveData;
import androidx.lifecycle.ViewModel;

import com.nycschools.a20220415_michaeladeniyi_nycschools.network.ApiResponse;
import com.nycschools.a20220415_michaeladeniyi_nycschools.network.model.SchoolModel;

import java.util.List;

public class SchoolNycViewModel extends ViewModel {
    private final SchoolRepository schoolsNYCRepo;
    private final MediatorLiveData<ApiResponse<List<SchoolModel>>> mediatorLiveData;

    public SchoolNycViewModel(){
        mediatorLiveData = new MediatorLiveData<>();
        schoolsNYCRepo = new SchoolRepository();
    }

    public LiveData<ApiResponse<List<SchoolModel>>> getSchools(){
        mediatorLiveData.addSource(schoolsNYCRepo.getSchoolsInNyc(), mediatorLiveData::setValue);
        return mediatorLiveData;
    }

}

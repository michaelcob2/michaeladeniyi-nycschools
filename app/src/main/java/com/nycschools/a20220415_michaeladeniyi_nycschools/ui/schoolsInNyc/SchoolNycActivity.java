package com.nycschools.a20220415_michaeladeniyi_nycschools.ui.schoolsInNyc;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.Observer;
import androidx.recyclerview.widget.LinearLayoutManager;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import com.google.android.material.snackbar.Snackbar;
import com.nycschools.BaseApplication;
import com.nycschools.a20220415_michaeladeniyi_nycschools.R;
import com.nycschools.a20220415_michaeladeniyi_nycschools.databinding.ActivitySchoolNycBinding;
import com.nycschools.a20220415_michaeladeniyi_nycschools.network.ApiResponse;
import com.nycschools.a20220415_michaeladeniyi_nycschools.network.model.SchoolModel;
import com.nycschools.a20220415_michaeladeniyi_nycschools.ui.schoolNyc.SchoolSatActivity;

import java.util.List;

public class SchoolNycActivity extends AppCompatActivity {

    private static final String TAG = SchoolNycActivity.class.getSimpleName();
    ActivitySchoolNycBinding binding;
    SchoolNycViewModel viewModel;
    SchoolsNYCAdapter adapter;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_school_nyc);
        init();
        //check for internet connections
        if (BaseApplication.getInstance().isNetworkAvailable(SchoolNycActivity.this)){
            viewModel = new SchoolNycViewModel();
            viewModel.getSchools().observe(this, (ApiResponse<List<SchoolModel>> schoolModels) ->{
                if (schoolModels == null) {
                    //handle error : add empty screen

                } else if (schoolModels.getError() == null) {
                    if (!schoolModels.data.isEmpty()) {
                        Log.e(TAG, "observe onChanged()=" + schoolModels.data.size());
                        binding.progressBar.setVisibility(View.GONE);
                        adapter.addSchoolList(schoolModels.data);
                        adapter.notifyDataSetChanged();
                    } else {
                        Toast.makeText(SchoolNycActivity.this, "School List is empty", Toast.LENGTH_SHORT).show();
                    }
                } else {
                    Throwable e = schoolModels.getError();
                    Snackbar snackbar = Snackbar
                            .make(binding.layout,  e.getMessage(), Snackbar.LENGTH_LONG)
                            .setAction("RETRY", view -> {
                                //call get data here again
                                //loadSchoolList();
                            });
                    snackbar.show();
                    Log.e(TAG, "Error is " + e.getLocalizedMessage());
                }

            });

        }else{
            Toast.makeText(this, "No Internet Access", Toast.LENGTH_SHORT).show();
        }
    }

    private void init() {
        SchoolsNYCAdapter.ItemClickListener itemClickListener =
                schoolModel -> startActivity(
                        SchoolSatActivity.getIntentStarted(SchoolNycActivity.this,
                                schoolModel.getDbn(), schoolModel.getSchoolName()));
        binding.rvSchools.setHasFixedSize(true);
        binding.rvSchools.setLayoutManager(new LinearLayoutManager(this));
        adapter = new SchoolsNYCAdapter(itemClickListener);
        binding.rvSchools.setAdapter(adapter);
    }
}
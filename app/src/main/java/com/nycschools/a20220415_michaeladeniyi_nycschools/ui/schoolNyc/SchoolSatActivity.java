package com.nycschools.a20220415_michaeladeniyi_nycschools.ui.schoolNyc;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import com.google.android.material.snackbar.Snackbar;
import com.nycschools.BaseApplication;
import com.nycschools.a20220415_michaeladeniyi_nycschools.R;
import com.nycschools.a20220415_michaeladeniyi_nycschools.databinding.ActivitySchoolSatBinding;
import com.nycschools.a20220415_michaeladeniyi_nycschools.network.model.SchoolSatModel;

public class SchoolSatActivity extends AppCompatActivity {

    private final String TAG = getClass().getSimpleName();
    public static final String INTENT_KEY = "dbn_key";
    public static final String INTENT_KEY_SCHOOL = "schoolname_key";

    ActivitySchoolSatBinding binding;

    public static Intent getIntentStarted(Context con, String dbn, String schoolName) {
        Intent intent = new Intent(con, SchoolSatActivity.class);
        intent.putExtra(INTENT_KEY, dbn);
        intent.putExtra(INTENT_KEY_SCHOOL, schoolName);
        return intent;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_school_sat);
        String dbnData = getIntent().getStringExtra(INTENT_KEY);
        String schoolName = getIntent().getStringExtra(INTENT_KEY_SCHOOL);

        if (BaseApplication.getInstance().isNetworkAvailable(this)) {
            //show progress bar
            SchoolSatViewModel schoolSatViewModel = new SchoolSatViewModel();
            schoolSatViewModel.getDataBysSchoolDBN(dbnData).observe(this, v -> {
                if (v.getError() == null) {
                    if (!v.data.isEmpty()) {
                        SchoolSatModel schoolSatModel = v.data.get(0);
                        binding.setSat(schoolSatModel);
                    } else {
                        Toast.makeText(SchoolSatActivity.this, "There is not response for this school ", Toast.LENGTH_SHORT).show();

                    }
                } else {
                    Throwable e = v.getError();
                    Toast.makeText(SchoolSatActivity.this, "Error is " + e.getMessage(), Toast.LENGTH_SHORT).show();
                    Log.e(TAG, "Error is " + e.getLocalizedMessage());
                }
            });
        } else {
            Snackbar snackbar = Snackbar
                    .make(binding.layout, "No Internet Connection ", Snackbar.LENGTH_LONG)
                    .setAction("RETRY", view -> {
                        //call get data here again
                    });
            snackbar.show();
        }
    }
}
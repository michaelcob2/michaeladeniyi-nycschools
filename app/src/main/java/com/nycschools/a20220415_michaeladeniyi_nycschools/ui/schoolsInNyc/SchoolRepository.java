package com.nycschools.a20220415_michaeladeniyi_nycschools.ui.schoolsInNyc;

import android.util.Log;

import androidx.lifecycle.MutableLiveData;

import com.nycschools.BaseApplication;
import com.nycschools.a20220415_michaeladeniyi_nycschools.network.ApiResponse;
import com.nycschools.a20220415_michaeladeniyi_nycschools.network.ApiService;
import com.nycschools.a20220415_michaeladeniyi_nycschools.network.model.SchoolModel;

import org.jetbrains.annotations.NotNull;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SchoolRepository {

    public final String TAG = SchoolRepository.class.getSimpleName();

    public MutableLiveData<ApiResponse<List<SchoolModel>>> getSchoolsInNyc() {

        final MutableLiveData<ApiResponse<List<SchoolModel>>> mutableLiveData = new MutableLiveData<>();


        ApiService apiService =
                BaseApplication.getRetrofitClient().create(ApiService.class);

        apiService.getSchoolsInNyc().enqueue(new Callback<List<SchoolModel>>() {
            @Override
            public void onResponse(Call<List<SchoolModel>> call,
                                   @NotNull Response<List<SchoolModel>> response) {
                Log.v(TAG, "response="+response );

                if (response.isSuccessful() && response.body()!=null ) {
                    Log.e(TAG, "response.size="+response.body().size());
                    mutableLiveData.postValue(new ApiResponse<>(response.body()));
                } else {
                    mutableLiveData.postValue(new ApiResponse<>(new Throwable(response.message())));
                }
            }

            @Override
            public void onFailure(Call<List<SchoolModel>> call, Throwable t) {
                Log.e(TAG, "onFailure" + call.toString());
                mutableLiveData.postValue(new ApiResponse<>(t));
            }
        });

        return mutableLiveData;
    }


}
